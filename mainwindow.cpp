#include "mainwindow.h"

#include <QDebug>
#include <QElapsedTimer>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QGraphicsDropShadowEffect>
#include <QTimer>
#include <QTransform>
#include <QWheelEvent>

static const int TIMER_PERIOD = 500;
static const qreal RADIUS = 1.0;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_scene(new QGraphicsScene(this)),
    m_view(new QGraphicsView)
{
    m_view->setScene(m_scene);
    m_view->installEventFilter(this);
    setCentralWidget(m_view);

    const qreal radius = RADIUS;
    const qreal diameter = 2*radius;
    auto item = m_scene->addEllipse(-radius, -radius, diameter, diameter);
    item->setPen(Qt::NoPen);
    item->setBrush(Qt::darkBlue);
    auto effect = new QGraphicsDropShadowEffect;
    effect->setColor(Qt::darkRed);
    item->setGraphicsEffect(effect);


    auto elapsedTimer = new QElapsedTimer;
    elapsedTimer->start();
    auto timer = new QTimer;
    timer->setInterval(TIMER_PERIOD);
    timer->connect(timer, &QTimer::timeout,
                   this, [this, elapsedTimer]() {
        static bool first = false;
        if (!first)
        {
            m_view->fitInView(m_view->scene()->itemsBoundingRect(), Qt::KeepAspectRatio);
            first = true;
            return;
        }
        static quint64 last = 0;
        quint64 now = elapsedTimer->elapsed();
        if (now - last > 2*TIMER_PERIOD)
            qDebug() << QString("Scaling: ellapsed=%1, m11=%2, radius=%3")
                     .arg(now - last).arg(m_view->transform().m11()).arg(RADIUS);
        last = now;
        m_view->scale(1.1, 1.1);
    });
    timer->start();

    m_paintTimer.start();
}

MainWindow::~MainWindow()
{
}


bool MainWindow::eventFilter(QObject *watched, QEvent *event)
{
    if (watched == m_view && event->type() == QEvent::Paint)
    {
        static quint64 last = 0;
        quint64 now = m_paintTimer.elapsed();
        if (now - last > 2*TIMER_PERIOD)
            qDebug() << QString("paintEvent: ellapsed=%1").arg(now - last);
        last = now;
    }

    return QMainWindow::eventFilter(watched, event);
}
