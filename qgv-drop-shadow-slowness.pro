#-------------------------------------------------
#
# Project created by QtCreator 2016-11-26T22:03:44
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = qgv-drop-shadow-slowness
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h
